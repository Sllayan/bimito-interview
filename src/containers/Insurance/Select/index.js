import React from "react";
// dependencies
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
// static
import InsuranceIcon from "../../../static/Svgs/insurance.svg";
// local components
import Auxiliary from "../../../components/HOC/Auxiliary/Auxiliary";
import InsuranceBox from "../../../components/InsuranceBox";
// local requirements
import { setInsuranceAction } from "../../../store/actions/action";
// style
import styles from "./index.module.css";

const Select = (props) => {
  // inner functions
  const selectSection = (value) => {
    if (value !== "forbiden") {
      props._setInsurance(value);
      props.history.push("/insurance");
    }
  };

  return (
    <Auxiliary>
      <Helmet>
        <title>Bimito : select</title>
      </Helmet>

      <div className={styles.pickup_wrapper}>
        <InsuranceBox
          image={InsuranceIcon}
          title="شخص ثالث"
          onClick={(value) => selectSection(value)}
        />
        <InsuranceBox
          image={InsuranceIcon}
          title="بدنه"
          disabled={true}
          onClick={(value) => selectSection(value)}
        />
      </div>
    </Auxiliary>
  );
};

const mapStateToProps = (state) => {
  return {
    userInfoState: state.userInfoState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _setInsurance: (data) => dispatch(setInsuranceAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Select);
