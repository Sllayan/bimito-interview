/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
// dependencies
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
// local components
import Auxiliary from "../../../components/HOC/Auxiliary/Auxiliary";
import InsuranceTypeSection from "./Sections/InsuranceTypeSection/index";
import InsuranceCompanySection from "./Sections/InsuranceCompanySection";
import SaleOffSection from "./Sections/SaleOffSection";
import Result from "../../../components/Result/index";
// style
import styles from "./index.module.css";

const Transmitter = (props) => {
  // Hooks
  const whichSection = {
    INSURANCE_TYPE: "type",
    INSURANCE_COMPANY: "company",
    OFF_PERCENT: "off",
  };
  //// active page
  const [activePage, setActivePage] = useState(whichSection.INSURANCE_TYPE);
  //// show modal
  const [modalActive, setModalActive] = useState(false);
  //// refresh
  useEffect(() => {
    if (props.InsuranceType === null) {
      props.history.push("/select");
    }
  }, []);

  // inner functions
  const whichModal = () => {
    setModalActive(true);
  };

  return (
    <Auxiliary>
      <Helmet>
        <title>Bimito : Insurance</title>
      </Helmet>

      {
        // did user select an insurance ?
        !!props.InsuranceType ? (
          <Auxiliary>
            {activePage === whichSection.INSURANCE_TYPE && (
              <InsuranceTypeSection
                page_title={props.InsuranceType.InsuranceType}
                nextPage={() => setActivePage(whichSection.INSURANCE_COMPANY)}
                prevPage={() => props.history.push("/select")}
              />
            )}

            {activePage === whichSection.INSURANCE_COMPANY && (
              <InsuranceCompanySection
                page_title={props.InsuranceType.InsuranceType}
                nextPage={() => setActivePage(whichSection.OFF_PERCENT)}
                prevPage={() => setActivePage(whichSection.INSURANCE_TYPE)}
              />
            )}

            {activePage === whichSection.OFF_PERCENT && (
              <SaleOffSection
                page_title={props.InsuranceType.InsuranceType}
                showValues={whichModal}
              />
            )}

            <div
              onClickCapture={() => setModalActive(false)}
              className={[
                styles.popup_wrapper,
                modalActive ? styles.show_popup_wrapper : null,
              ].join(" ")}
            >
              <div
                className={[
                  styles.user_list_description,
                  modalActive ? styles.show_user_list_description : null,
                ].join(" ")}
              >
                {activePage === whichSection.OFF_PERCENT ? <Result /> : null}
              </div>
            </div>
          </Auxiliary>
        ) : null
      }
    </Auxiliary>
  );
};

const mapStateToProps = (state) => {
  return {
    userInfoState: state.userInfoState,
    InsuranceType: state.InsuranceType,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    //
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Transmitter);
