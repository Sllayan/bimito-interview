/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
// dependencies
import PropTypes from "prop-types";
import { connect } from "react-redux";
// local components
import Button from "../../../../../components/Button";
import SelectBox from "../../../../../components/Inputs/SelectBox";
import { validateCompany } from "../../validate";
// local requirements
import {
  getCompaniesAction,
  setInsuranceCompanyAction,
} from "../../../../../store/actions/action";
import { api } from "../../../../../store/apis/api";
// style
import styles from "./index.module.css";

const InsuranceCompany = (props) => {
  // Hooks
  const [insuranceCompany, setInsuranceCompany] = useState(
    props.insuranceCompany !== null
      ? props.insuranceCompany.insuranceCompany
      : ""
  );
  const [insuranceCompanyError, setInsuranceCompanyError] = useState("");
  // fetch from server
  useEffect(() => {
    let done = true;
    let companiesList = [];
    api()
      .get("/core/data/companies")
      .then((res) => {
        if (done) {
          res.data.result.forEach((Type) => {
            companiesList.push(Type.company);
          });
          props._getCompanies(companiesList);
        }
      });

    return () => {
      done = false;
    };
  }, []);

  // inner functions
  const nextPageHandler = () => {
    let errorActive = false;
    errorActive = validateCompany(
      insuranceCompany,
      setInsuranceCompanyError,
      errorActive
    );
    if (!errorActive) {
      props.nextPage();
      props._setInsuranceCompany(insuranceCompany);
    }
  };

  return (
    <div className={styles.page_wrapper}>
      <h1 className={styles.page_title}> بیمه {props.page_title} </h1>
      <p className={styles.description}>
        {" "}
        شرکت بیمه گر قبلی خود را در این بخش وارد کنید{" "}
      </p>

      <div className={styles.inline_inputs}>
        <SelectBox
          placeHolder="شرکت بیمه گر قبلی"
          error={insuranceCompanyError}
          onChange={(val) => setInsuranceCompany(val)}
          loopOn={
            props.companiesList !== null
              ? props.companiesList.companiesList
              : []
          }
          defaultValue={insuranceCompany}
        />
      </div>

      <div className={styles.inline_buttons}>
        <Button
          rightArrow={true}
          text="بازگشت"
          onClick={props.prevPage}
          color="white"
        />
        <Button
          leftArrow={true}
          text="مرحله بعد"
          onClick={nextPageHandler}
          color="white"
        />
      </div>
    </div>
  );
};

InsuranceCompany.propTypes = {
  page_title: PropTypes.string.isRequired,
  nextPage: PropTypes.func.isRequired,
  prevPage: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    companiesList: state.companiesList,
    insuranceCompany: state.insuranceCompany,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _getCompanies: (data) => dispatch(getCompaniesAction(data)),
    _setInsuranceCompany: (data) => dispatch(setInsuranceCompanyAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InsuranceCompany);
