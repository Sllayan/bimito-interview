/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
// dependencies
import PropTypes from "prop-types";
import { connect } from "react-redux";
// local components
import Button from "../../../../../components/Button";
import SelectBox from "../../../../../components/Inputs/SelectBox";
import { validateCarInsurance } from "../../validate";
// local requirements
import {
  getCarModalAction,
  getCarTypesAction,
  getAllCarsAction,
  setUserCarInfoAction,
} from "../../../../../store/actions/action";
import { api } from "../../../../../store/apis/api";
// style
import styles from "./index.module.css";

const InsuranceTypeSection = (props) => {
  // Hooks
  //// car type
  const [carType, setCarType] = useState(
    props.userCarInfo.carType !== null ? props.userCarInfo.carType : ""
  );
  //// car type error
  const [carTypeError, setCarTypeError] = useState("");
  //// car modal
  const [carModal, setCarModal] = useState(
    props.userCarInfo.carModal !== null ? props.userCarInfo.carModal : ""
  );
  //// car modal error
  const [carModalError, setCarModalError] = useState("");

  useEffect(() => {
    let done = true;
    let carTypeList = [];
    api()
      .get("/core/data/third-car-types")
      .then((res) => {
        if (done) {
          props._getAllCars(res.data.result);
          res.data.result.forEach((Type) => {
            carTypeList.push(Type.carType);
          });
          props._getCarTypes(carTypeList);
        }
      });

    return () => {
      done = false;
    };
  }, []);

  // fetch from server
  useEffect(() => {
    if (carType !== props.userCarInfo.carType) {
      setCarModal("");
    }

    let carModalList = [];
    if (carType !== "") {
      let item = props.allCars.allCars.filter(
        (item) => item.carType === carType
      );

      item[0].brand.forEach((Type) => {
        carModalList.push(Type.name);
      });
      props._getCarModals(carModalList);
    }
  }, [carType]);

  // inner functions
  const nextPageHandler = () => {
    let errorActive = false;

    errorActive = validateCarInsurance(
      carType,
      carModal,
      setCarTypeError,
      setCarModalError,
      errorActive
    );
    if (!errorActive) {
      props.nextPage();

      let data = {
        carType: carType,
        carModal: carModal,
      };

      props._setUserCarInfo(data);
    }
  };

  return (
    <div className={styles.page_wrapper}>
      <h1 className={styles.page_title}> بیمه {props.page_title} </h1>
      <p className={styles.description}>نوع و مدل خودروی خود را انتخاب کنید</p>

      <div className={styles.inline_inputs}>
        <div className={styles.ii_small_input_wrapper}>
          <SelectBox
            placeHolder="نوع خودرو"
            error={carTypeError}
            onChange={(val) => setCarType(val)}
            loopOn={props.carTypes !== null ? props.carTypes.carTypes : []}
            defaultValue={carType}
          />
        </div>
        <div className={styles.ii_small_input_wrapper}>
          <SelectBox
            placeHolder="مدل خودرو"
            error={carModalError}
            onChange={(val) => setCarModal(val)}
            loopOn={props.carModal !== null ? props.carModal.carModal : []}
            disabled={carType === ""}
            defaultValue={carModal}
          />
        </div>
      </div>

      <div className={styles.inline_buttons}>
        <Button
          rightArrow={true}
          text="بازگشت"
          onClick={props.prevPage}
          color="white"
        />
        <Button
          leftArrow={true}
          text="مرحله بعد"
          onClick={nextPageHandler}
          color="white"
        />
      </div>
    </div>
  );
};

InsuranceTypeSection.propTypes = {
  page_title: PropTypes.string.isRequired,
  nextPage: PropTypes.func.isRequired,
  prevPage: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    allCars: state.allCars,
    carTypes: state.carTypes,
    carModal: state.carModal,
    userCarInfo: state.UserCarInfo,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _getCarTypes: (data) => dispatch(getCarTypesAction(data)),
    _getCarModals: (data) => dispatch(getCarModalAction(data)),
    _setUserCarInfo: (data) => dispatch(setUserCarInfoAction(data)),
    _getAllCars: (data) => dispatch(getAllCarsAction(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsuranceTypeSection);
