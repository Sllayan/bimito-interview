/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
// dependencies
import PropTypes from "prop-types";
import { connect } from "react-redux";
// local components
import Button from "../../../../../components/Button";
import SelectBox from "../../../../../components/Inputs/SelectBox";
// local requirements
import { validatePercent } from "../../validate";
import {
  getPercentListAction,
  setPercentAction,
} from "../../../../../store/actions/action";
import { api } from "../../../../../store/apis/api";
// style
import styles from "./index.module.css";
// Hooks
const SaleOffSection = (props) => {
  const [off, setOff] = useState(
    props.offPercent.offPercent !== null ? props.offPercent.offPercent : ""
  );
  const [offError, setOffError] = useState("");
  const [offDriverPercent, setOffDriverPercent] = useState(
    props.offPercent.offDriverPercent !== null
      ? props.offPercent.offDriverPercent
      : ""
  );
  const [offDriverPercentError, setOffDriverPercentError] = useState("");

  useEffect(() => {
    let done = true;
    let offPercenrArray = [];
    api()
      .get("/core/data/car-third-discount")
      .then((res) => {
        if (done) {
          res.data.result.forEach((Type) => {
            offPercenrArray.push(Type.title);
          });
          props._getPercentList(offPercenrArray);
        }
      });

    return () => {
      done = false;
    };
  }, []);

  // inner functions
  const nextPageHandler = () => {
    let errorActive = false;

    errorActive = validatePercent(
      off,
      setOffError,
      offDriverPercent,
      setOffDriverPercentError,
      errorActive
    );
    if (!errorActive) {
      let data = {
        offPercent: off,
        offDriverPercent: offDriverPercent,
      };

      props._setOffPercent(data);
      props.showValues();
    }
  };

  return (
    <div className={styles.page_wrapper}>
      <h1 className={styles.page_title}> بیمه {props.page_title} </h1>
      <p className={styles.description}>
        {" "}
        درصد تخفیف بیمه شخص ثالث و حوادث راننده را تعیین کنید{" "}
      </p>

      <div className={styles.block_inputs}>
        <SelectBox
          placeHolder="درصد تخفیف ثالث"
          error={offError}
          onChange={(val) => setOff(val)}
          loopOn={
            props.offPercentList !== null
              ? props.offPercentList.offPercentList
              : []
          }
          defaultValue={off}
        />
      </div>
      <div className={styles.block_inputs}>
        <SelectBox
          placeHolder="درصد تخفیف حوادث راننده"
          error={offDriverPercentError}
          onChange={(val) => setOffDriverPercent(val)}
          loopOn={
            props.offPercentList !== null
              ? props.offPercentList.offPercentList
              : []
          }
          defaultValue={offDriverPercent}
        />
      </div>

      <div className={styles.leftButton}>
        <Button text="استعلام قیمت" onClick={nextPageHandler} />
      </div>
    </div>
  );
};

SaleOffSection.propTypes = {
  page_title: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  return {
    offPercentList: state.offPercentList,
    offPercent: state.offPercent,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _getPercentList: (data) => dispatch(getPercentListAction(data)),
    _setOffPercent: (data) => dispatch(setPercentAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SaleOffSection);
