export const validateCarInsurance = (
  carType,
  carModal,
  setCarTypeError,
  setCarModalError,
  errorActive
) => {
  if (carType === "") {
    setCarTypeError("انتخاب نوع خودرو الزامی است");
    errorActive = true;
  } else {
    setCarTypeError("");
  }

  if (carModal === "") {
    setCarModalError("انتخاب مدل خودرو الزامی است");
    errorActive = true;
  } else {
    setCarModalError("");
  }

  return errorActive;
};

export const validateCompany = (
  insuranceCompany,
  setInsuranceCompanyError,
  errorActive
) => {
  if (insuranceCompany === "") {
    setInsuranceCompanyError("شرکت بیمه قبلی خود را انتخاب کنید");
    errorActive = true;
  } else {
    setInsuranceCompanyError("");
  }

  return errorActive;
};

export const validatePercent = (
  offPercent,
  setOffPercentError,
  offDriverPercent,
  setOffDriverPercentError,
  errorActive
) => {
  if (offPercent === "") {
    setOffPercentError("درصد تخفیف ثالث را انتخاب کنید");
    errorActive = true;
  } else {
    setOffPercentError("");
  }

  if (offDriverPercent === "") {
    setOffDriverPercentError("درصد تخفیف حوادث راننده را انتخاب کنید");
    errorActive = true;
  } else {
    setOffDriverPercentError("");
  }

  return errorActive;
};
