/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";
// dependencies
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
// local components
import Auxiliary from "../../components/HOC/Auxiliary/Auxiliary";
import Input from "../../components/Inputs/Input/index";
import Button from "../../components/Button";
// local requirements
import { loginValidate } from "./validate";
import { setUserInfoAction } from "../../store/actions/action";
// style
import styles from "./index.module.css";

const Login = (props) => {
  // Hooks
  //// form values
  const [loginForm, setLoginForm] = useState({
    user_name: "",
    last_name: "",
    phone_number: "",
    password: "",
  });
  //// validation form values
  const [loginFormError, setLoginFormError] = useState({
    user_name_error: "",
    last_name_error: "",
    phone_number_error: "",
    password_error: "",
  });
  //// reset form values
  useEffect(() => {
    return () => {
      setLoginForm({
        user_name: "",
        last_name: "",
        phone_number: "",
        password: "",
      });
      setLoginFormError({
        user_name_error: "",
        last_name_error: "",
        phone_number_error: "",
        password_error: "",
      });
    };
  }, []);

  // inner functions
  //// set value in form values
  const changeInputValue = (value, id) => {
    Object.keys(loginForm).map((item) => {
      if (item === id) {
        setLoginForm((prevState) => ({
          ...prevState,
          [id]: value,
        }));
      }
    });
  };
  //// submit form
  const submitForm = () => {
    let errorActive = false;
    errorActive = loginValidate(
      loginForm.user_name,
      loginForm.last_name,
      loginForm.phone_number,
      loginForm.password,
      loginFormError,
      setLoginFormError,
      errorActive
    );
    if (!errorActive) {
      props._setUserInformation(loginForm);
      props.history.push("/select");
    }
  };

  return (
    <Auxiliary>
      <Helmet>
        <title>Bimito : Login</title>
      </Helmet>
      <div className={styles.form_wrapper}>
        <h1 className={styles.page_title}> ثبت نام </h1>

        <div className={styles.inline_inputs}>
          <div className={styles.ii_small_input_wrapper}>
            <Input
              onChange={(val) => changeInputValue(val, "user_name")}
              placeHolder="نام"
              type="text"
              value={loginForm.user_name}
              error={loginFormError.user_name_error}
            />
          </div>
          <div className={styles.ii_small_input_wrapper}>
            <Input
              onChange={(val) => changeInputValue(val, "last_name")}
              placeHolder="نام خانوادگی"
              type="text"
              value={loginForm.last_name}
              error={loginFormError.last_name_error}
            />
          </div>
        </div>

        <div className={styles.block_inputs}>
          <Input
            onChange={(val) => changeInputValue(val, "phone_number")}
            placeHolder="شماره موبایل"
            type="tel"
            value={loginForm.phone_number}
            error={loginFormError.phone_number_error}
            direction="ltr"
            maxLength={13}
          />
        </div>

        <div className={styles.block_inputs}>
          <Input
            onChange={(val) => changeInputValue(val, "password")}
            placeHolder="رمز عبور"
            type="password"
            value={loginForm.password}
            error={loginFormError.password_error}
          />
        </div>

        <div className={styles.button_wrapper}>
          <Button text="ثبت نام" onClick={submitForm} color="green" />
        </div>
      </div>
    </Auxiliary>
  );
};

const mapStateToProps = (state) => {
  return {
    //
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _setUserInformation: (data) => dispatch(setUserInfoAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
