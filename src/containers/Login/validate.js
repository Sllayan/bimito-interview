/* eslint-disable array-callback-return */
// local requirements
import { updateObject } from "../../shared/utils";

export const loginValidate = (
  user_name,
  last_name,
  phone_number,
  password,
  loginFormError,
  setLoginFormError,
  errorActive
) => {
  // user_name validation
  if (user_name.length > 0) {
    let user_name_is_farsi = persianActive(user_name);
    if (user_name_is_farsi) {
      updateObject(loginFormError, setLoginFormError, "user_name_error", "");
    } else {
      updateObject(
        loginFormError,
        setLoginFormError,
        "user_name_error",
        "نام را به فارسی وارد کنید"
      );
      errorActive = true;
    }
  } else {
    updateObject(
      loginFormError,
      setLoginFormError,
      "user_name_error",
      "وارد کردن نام الزامی است"
    );
    errorActive = true;
  }

  // last_name validation
  if (last_name.length > 0) {
    let last_name_is_farsi = persianActive(last_name);
    if (last_name_is_farsi) {
      updateObject(loginFormError, setLoginFormError, "last_name_error", "");
    } else {
      updateObject(
        loginFormError,
        setLoginFormError,
        "last_name_error",
        "نام خانوادگی را به فارسی وارد کنید"
      );
      errorActive = true;
    }
  } else {
    updateObject(
      loginFormError,
      setLoginFormError,
      "last_name_error",
      "وارد کردن نام خانوادگی الزامی است"
    );
    errorActive = true;
  }

  // phone_number validation
  if (phone_number.length > 0) {
    const phone_number_regex = /^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$/;
    if (phone_number_regex.test(phone_number)) {
      updateObject(loginFormError, setLoginFormError, "phone_number_error", "");
    } else {
      updateObject(
        loginFormError,
        setLoginFormError,
        "phone_number_error",
        "شماره به شکل نادرستی وارد شده"
      );
      errorActive = true;
    }
  } else {
    updateObject(
      loginFormError,
      setLoginFormError,
      "phone_number_error",
      "وارد کردن شماره الزامی است"
    );
    errorActive = true;
  }

  // password validation
  if (password.length > 0) {
    const password_regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{4,10}$/;
    if (password_regex.test(password)) {
      updateObject(loginFormError, setLoginFormError, "password_error", "");
    } else {
      if (password.length < 4) {
        updateObject(
          loginFormError,
          setLoginFormError,
          "password_error",
          "حداقل 4 کراکتر نیاز است"
        );
        errorActive = true;
      } else if (password.length > 10) {
        updateObject(
          loginFormError,
          setLoginFormError,
          "password_error",
          "حداقل 10 کراکتر نیاز است"
        );
        errorActive = true;
      } else {
        updateObject(
          loginFormError,
          setLoginFormError,
          "password_error",
          "حداقل یک حرف بزرگ، یک حرف کوچک، یک عدد الزامی است"
        );
        errorActive = true;
      }
    }
  } else {
    updateObject(loginFormError, setLoginFormError, "password_error", "");
    errorActive = true;
  }

  return errorActive;
};

export const persianActive = (item) => {
  let text_array = item.split("");
  let persianActive = null;
  for (let i = 0; i < text_array.length; i++) {
    if (
      text_array[i] === "آ" ||
      text_array[i] === "ا" ||
      text_array[i] === "ب" ||
      text_array[i] === "پ" ||
      text_array[i] === "ت" ||
      text_array[i] === "ث" ||
      text_array[i] === "ج" ||
      text_array[i] === "چ" ||
      text_array[i] === "ح" ||
      text_array[i] === "خ" ||
      text_array[i] === "د" ||
      text_array[i] === "ذ" ||
      text_array[i] === "ر" ||
      text_array[i] === "ز" ||
      text_array[i] === "ژ" ||
      text_array[i] === "س" ||
      text_array[i] === "ش" ||
      text_array[i] === "ص" ||
      text_array[i] === "ض" ||
      text_array[i] === "ط" ||
      text_array[i] === "ظ" ||
      text_array[i] === "ع" ||
      text_array[i] === "غ" ||
      text_array[i] === "ف" ||
      text_array[i] === "ق" ||
      text_array[i] === "ک" ||
      text_array[i] === "گ" ||
      text_array[i] === "ل" ||
      text_array[i] === "م" ||
      text_array[i] === "ن" ||
      text_array[i] === "و" ||
      text_array[i] === "ه" ||
      text_array[i] === "ی" ||
      text_array[i] === "ء" ||
      text_array[i] === "ئ" ||
      text_array[i] === " "
    ) {
      persianActive = true;
    } else {
      persianActive = false;
      break;
    }
  }

  return persianActive;
};
