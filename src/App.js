import React from "react";
// dependencies
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
// local components
import Layout from "./components/HOC/Layout/Layout";
import Login from "./containers/Login";
import Select from "./containers/Insurance/Select";
import Transmitter from "./containers/Insurance/Transmitter";

function App(props) {
  let routes = (
    <Switch>
      <Route path="/" exact component={Login} />
      <Route path="/select" exact component={Select} />
      <Route path="/insurance" exact component={Transmitter} />
    </Switch>
  );

  return <Layout user={props.userInfoState}>{routes}</Layout>;
}

const mapStateToProps = (state) => {
  return {
    userInfoState: state.userInfoState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    //
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
