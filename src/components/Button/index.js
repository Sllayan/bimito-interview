import React from "react";
// dependencies
import PropTypes from "prop-types";
// static
import ArrowIcon from "../../static/Svgs/arrow.svg";
// style
import styles from "./index.module.css";

const Button = (props) => {
  return (
    <button
      className={[
        styles.button,
        props.color === "white"
          ? styles.b_white_button
          : props.color === "green"
          ? styles.b_green_button
          : null,
      ].join(" ")}
      onClick={props.onClick}
    >
      {props.rightArrow ? (
        <span className={styles.right_arrow}>
          <img src={ArrowIcon} alt="right" />
        </span>
      ) : null}

      {props.text}

      {props.leftArrow ? (
        <span className={styles.left_arrow}>
          <img src={ArrowIcon} alt="left" />
        </span>
      ) : null}
    </button>
  );
};

Button.propTypes = {
  rightArrow: PropTypes.bool,
  leftArrow: PropTypes.bool,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  color: PropTypes.oneOf(["white", "green"]),
};

Button.defaultProps = {
  rightArrow: false,
  leftArrow: false,
  text: "کلیک کنید",
  color: "green",
};

export default Button;
