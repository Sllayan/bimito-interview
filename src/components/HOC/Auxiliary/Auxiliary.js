import React from "react";

const Auxiliary = (props) => <React.Fragment>{props.children}</React.Fragment>;

export default Auxiliary;
