import React from "react";
// dependencies
import PropTypes from "prop-types";
// static
import UserImage from "../../../static/Svgs/user.svg";
import Logo from "../../../static/Svgs/logo.svg";
import Car from "../../../static/Svgs/car-green.svg";
// local component
import Auxiliary from "../Auxiliary/Auxiliary";
// style
import styles from "./Layout.module.css";

const Layout = (props) => {
  return (
    <div className={styles.layout_wrapper}>
      <div className={styles.navbar}>
        <span className={styles.navbar_logo}>
          <img src={Logo} alt="لوگو تست بیمیتو" />
        </span>

        <h2 className={styles.navbar_title}>
          {" "}
          سامانه مقایسه و خرید آنلاین بیمه{" "}
        </h2>

        <span className={styles.navbar_item}>
          {props.user.user_name !== null && props.user.last_name !== null ? (
            <Auxiliary>
              <div className={styles.iconWrapper}>
                <img src={UserImage} alt={`${props.user.user_name} icon`} />
              </div>
              <p>
                {" "}
                {props.user.user_name} {props.user.last_name}{" "}
              </p>
            </Auxiliary>
          ) : (
            <p> ثبت نام </p>
          )}
        </span>
      </div>

      <div className={styles.ColorPart}></div>

      <div className={styles.carImageWrapper}>
        <img src={Car} alt="bimito car" />
      </div>

      <div className={styles.contentChildren}>{props.children}</div>
    </div>
  );
};

Layout.propTypes = {
  user: PropTypes.object,
};

Layout.defaultProps = {
  user: {
    user_name: null,
    last_name: null,
    phone_number: null,
    password: null,
  },
};

export default Layout;
