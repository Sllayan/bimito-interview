import React, { useState } from "react";
// dependencies
import PropTypes from "prop-types";
// local components
import Auxiliary from "../../HOC/Auxiliary/Auxiliary";
// static
import downArrow from "../../../static/Svgs/downArrow.svg";
// style
import styles from "./index.module.css";

const SelectBox = (props) => {
  // Hooks
  const [showOptions, setShowOptions] = useState(false);
  const [selected, setSelected] = useState("");

  // inner functions
  const settingValue = (e) => {
    setSelected(e.currentTarget.dataset.item);
    props.onChange(e.currentTarget.dataset.item);
    setShowOptions(false);
  };
  const toggleOption = () => {
    if (!props.disabled) {
      setShowOptions(!showOptions);
    }
  };

  return (
    <Auxiliary>
      <div className={styles.select_box_wrapper}>
        <span onClick={toggleOption} className={styles.down_arrow}>
          <img src={downArrow} alt="down" />
        </span>

        <button
          onClick={toggleOption}
          className={[
            styles.select_box,
            selected === "" && !!!props.defaultValue
              ? styles.placeholder
              : null,
          ].join(" ")}
        >
          {selected === ""
            ? !!props.defaultValue
              ? props.defaultValue
              : props.placeHolder
            : selected}
        </button>

        {showOptions && !props.disabled && (
          <div className={styles.option_values_box}>
            {props.loopOn.length > 0 ? (
              props.loopOn.map((item, i) => (
                <span
                  key={i}
                  onClick={(e) => settingValue(e)}
                  className={styles.option_value}
                  data-item={item}
                >
                  {item}
                </span>
              ))
            ) : (
              <span className={styles.option_value}>موردی وجود ندارد</span>
            )}
          </div>
        )}

        {props.error.length > 0 ? (
          <p className={styles.error}>
            <span className={styles.error_star}>*</span> {props.error}{" "}
            <span className={styles.error_star}>*</span>
          </p>
        ) : null}
      </div>
    </Auxiliary>
  );
};

SelectBox.propTypes = {
  placeHolder: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
};

SelectBox.defaultProps = {
  placeHolder: "",
  error: "",
  defaultValue: null,
  disabled: false,
};

export default SelectBox;
