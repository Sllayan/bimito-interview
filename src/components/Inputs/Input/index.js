import React from "react";
// dependencies
import PropTypes from "prop-types";
// style
import styles from "./index.module.css";

const Input = (props) => {
  const setValue = (e) => {
    e.preventDefault();
    props.onChange(e.target.value);
  };

  return (
    <div className={styles.block_inputs}>
      <input
        className={[
          styles.input,
          props.direction === "ltr" && props.value.length > 0
            ? styles.ltr
            : null,
        ].join(" ")}
        placeholder={props.placeHolder}
        type={props.type}
        value={props.value}
        onChange={(e) => setValue(e)}
        maxLength={props.maxLength}
      />
      {props.error.length > 0 ? (
        <p className={styles.error}>
          <span className={styles.error_star}>*</span> {props.error}{" "}
          <span className={styles.error_star}>*</span>
        </p>
      ) : null}
    </div>
  );
};

Input.propTypes = {
  placeHolder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  direction: PropTypes.oneOf(["rtl", "ltr"]),
  maxLength: PropTypes.number,
};

Input.defaultProps = {
  placeHolder: "",
  type: "text",
  value: "",
  error: "",
  onChange: function () {},
};

export default Input;
