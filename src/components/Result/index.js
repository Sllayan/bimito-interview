import React from "react";
// dependencies
import { connect } from "react-redux";
// local components
import Auxiliary from "../HOC/Auxiliary/Auxiliary";
// style
import styles from "./index.module.css";

const Result = (props) => {
  return (
    <Auxiliary>
      <span className={styles.close}>&#xd7;</span>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> نام : </h5>
        <p> {props.userInfoState.user_name} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> نام خانوادگی : </h5>
        <p> {props.userInfoState.last_name} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> شماره موبایل : </h5>
        <p> {props.userInfoState.phone_number} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> رمز عبور : </h5>
        <p> {props.userInfoState.password} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> نوع بیمه : </h5>
        <p> {props.insuranceType} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> نوع خودرو : </h5>
        <p> {props.userCarInfo.carType} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> مدل خودرو : </h5>
        <p> {props.userCarInfo.carModal} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> شرکت بیمه گر قبلی : </h5>
        <p> {props.insuranceCompany.insuranceCompany} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> درصد تخفیف ثالث : </h5>
        <p> {props.offPercent.offPercent} </p>
      </div>
      <div className={styles.list_row}>
        <h5 className={styles.list_title}> درصد تخفیف حوادث راننده : </h5>
        <p> {props.offPercent.offDriverPercent} </p>
      </div>
    </Auxiliary>
  );
};

const mapStateToProps = (state) => {
  return {
    userInfoState: state.userInfoState,
    insuranceType: state.InsuranceType.InsuranceType,
    userCarInfo: state.UserCarInfo,
    insuranceCompany: state.insuranceCompany,
    offPercent: state.offPercent,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    //
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Result);
