import React from "react";
// dependencies
import PropTypes from "prop-types";
// style
import styles from "./index.module.css";

const InsuranceBox = (props) => {
  // inner functions
  const setSelectedValue = () => {
    if (!props.disabled) {
      props.onClick(props.title);
    } else {
      props.onClick("forbiden");
    }
  };

  return (
    <div
      className={[
        styles.box_wrapper,
        props.disabled ? styles.disabled : null,
      ].join(" ")}
      onClick={setSelectedValue}
    >
      <img src={props.image} alt={props.title} />
      <p> {props.title} </p>
    </div>
  );
};

InsuranceBox.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

InsuranceBox.defaultProps = {
  image: null,
  title: null,
  disabled: false,
};

export default InsuranceBox;
