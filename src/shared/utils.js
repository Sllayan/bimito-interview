/* eslint-disable array-callback-return */
export const updateObject = (obj, setObj, key, value) => {
  Object.keys(obj).map((item) => {
    if (item === key) {
      setObj((prevState) => ({
        ...prevState,
        [key]: value,
      }));
    }
  });
};
