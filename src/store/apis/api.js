import axios from "axios";

const BASE_URL = "https://bimename.com/bimename";

export function api() {
  const api = axios.create({
    baseURL: BASE_URL,
    timeout: 4000,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

  api.interceptors.request.use(
    function (config) {
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
  return api;
}
