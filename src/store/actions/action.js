import * as actionType from "./actionType";
// GET
export const getCarTypesAction = (data) => {
  return { type: actionType.GET_CAR_TYPES, data };
};

export const getCarModalAction = (data) => {
  return { type: actionType.GET_CAR_MODELS, data };
};

export const getCompaniesAction = (data) => {
  return { type: actionType.GET_COMPANIES, data };
};

export const getAllCarsAction = (data) => {
  return { type: actionType.GET_ALL_CARS, data };
};

export const getPercentListAction = (data) => {
  return { type: actionType.GET_OFF_PERCENT_LIST, data };
};
// SET
export const setUserCarInfoAction = (data) => {
  return { type: actionType.SET_USER_CAR_INFO, data };
};

export const setUserInfoAction = (data) => {
  return { type: actionType.SET_USER_INFO, data };
};

export const setInsuranceAction = (data) => {
  return { type: actionType.SET_INSURANCE, data };
};

export const setInsuranceCompanyAction = (data) => {
  return { type: actionType.SET_USER_INSURANCE_COMPANY, data };
};

export const setPercentAction = (data) => {
  return { type: actionType.SET_USER_OFF_PERCENT, data };
};
