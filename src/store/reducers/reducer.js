import * as actionType from "../actions/actionType";
import { updateObj } from "../utils/update";

const initialState = {
  userInfoState: {
    user_name: null,
    last_name: null,
    phone_number: null,
    password: null,
  },
  UserCarInfo: {
    carType: null,
    carModal: null,
  },
  offPercent: {
    offPercent: null,
    offDriverPercent: null,
  },
  InsuranceType: null,
  allCars: null,
  carTypes: null,
  carModal: null,
  companiesList: null,
  offPercentList: null,
  insuranceCompany: null,
};

const setUserInfo = (state, action) => {
  const updateValue = {
    user_name: action.data.user_name,
    last_name: action.data.last_name,
    phone_number: action.data.phone_number,
    password: action.data.password,
  };
  return updateObj(state, { userInfoState: updateValue });
};

const setInsurance = (state, action) => {
  const updateValue = {
    InsuranceType: action.data,
  };
  return updateObj(state, { InsuranceType: updateValue });
};

const getCarTypes = (state, action) => {
  const updateValue = {
    carTypes: action.data,
  };
  return updateObj(state, { carTypes: updateValue });
};

const getcarModal = (state, action) => {
  const updateValue = {
    carModal: action.data,
  };
  return updateObj(state, { carModal: updateValue });
};

const allCars = (state, action) => {
  const updateValue = {
    allCars: action.data,
  };
  return updateObj(state, { allCars: updateValue });
};

const getcompaniesList = (state, action) => {
  const updateValue = {
    companiesList: action.data,
  };
  return updateObj(state, { companiesList: updateValue });
};

const setInsuranceCompany = (state, action) => {
  const updateValue = {
    insuranceCompany: action.data,
  };
  return updateObj(state, { insuranceCompany: updateValue });
};

const offPercentList = (state, action) => {
  const updateValue = {
    offPercentList: action.data,
  };
  return updateObj(state, { offPercentList: updateValue });
};

const setUserCarInfo = (state, action) => {
  const updateValue = {
    carType: action.data.carType,
    carModal: action.data.carModal,
  };
  return updateObj(state, { UserCarInfo: updateValue });
};

const offPercent = (state, action) => {
  const updateValue = {
    offPercent: action.data.offPercent,
    offDriverPercent: action.data.offDriverPercent,
  };
  return updateObj(state, { offPercent: updateValue });
};

const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_USER_INFO:
      return setUserInfo(state, action);

    case actionType.SET_INSURANCE:
      return setInsurance(state, action);

    case actionType.GET_CAR_TYPES:
      return getCarTypes(state, action);

    case actionType.GET_CAR_MODELS:
      return getcarModal(state, action);

    case actionType.GET_ALL_CARS:
      return allCars(state, action);

    case actionType.GET_COMPANIES:
      return getcompaniesList(state, action);

    case actionType.SET_USER_INSURANCE_COMPANY:
      return setInsuranceCompany(state, action);

    case actionType.SET_USER_CAR_INFO:
      return setUserCarInfo(state, action);

    case actionType.GET_OFF_PERCENT_LIST:
      return offPercentList(state, action);

    case actionType.SET_USER_OFF_PERCENT:
      return offPercent(state, action);

    default:
      return state;
  }
};

export default Reducer;
